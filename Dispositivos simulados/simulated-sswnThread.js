/**
 * SIMULATED - Execution thread for speed sensors deployed on Raspberry Pi platform.
 * Created by Jesus Rodriguez, May 27, 2015.
 * Developed for DEPHISIT project.
 */

var util = require('util');
var sleep = require('sleep');

//var SpeedSensorBasedOnSerialOBD = require('./SpeedSensorBasedOnSerialOBD');

//var speedSensor = new SpeedSensorBasedOnSerialOBD('/dev/ttyUSB0', 38400);

console.log('simulated-sswnThread started');

setInterval(function() {
  //sswnPosition = speedSensor.getSpeed();
  sswnPosition = Math.floor(Math.random() * (50 - 0) + 0);
  console.log('simulated-sswnThread >> SPEED ' + util.inspect(sswnPosition, false, null));
  process.send(sswnPosition);
}, 1000);

