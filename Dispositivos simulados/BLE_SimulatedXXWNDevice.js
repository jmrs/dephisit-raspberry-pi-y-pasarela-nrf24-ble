/**
 * SIMULATED - Main code for create a BLE peripheral device of the DEPHISIT platform.
 * Created by Jesus Rodriguez, May 27, 2015.
 * Developed for DEPHISIT project.
 */

var util = require('util');

//Get information about operating system and cpu ------------------------------------------------------
/*var os = require('os');
console.log("\n--- Operating System Info ---------------------------");
console.log("Operating system platform: " + os.platform());
console.log("Operating system name: " + os.type());
console.log("Operating system release: " + os.release());
console.log("-----------------------------------------------------");
console.log("\n--- CPU Info ----------------------------------------");
console.log("CPU architecture: " + os.arch());
console.log("CPU endianess: " + os.endianness());
console.log("-----------------------------------------------------");*/
// ----------------------------------------------------------------------------------------------------

//Kill all instances of bluetoothd (needed to work bleno with BlueZ >= 5.17) --------------------------
var execSync = require('child_process').execSync;
console.log("Getting BlueZ version...");
var bluezVersion = parseFloat(execSync("bluetoothd -v"));
console.log("BlueZ version: " + bluezVersion);
if (bluezVersion >= 5.17) {
    console.log("Killing all instances of bluetoothd (needed to work bleno with BlueZ >= 5.17...");
    execSync("killall bluetoothd");
}
// ----------------------------------------------------------------------------------------------------

console.log('BLE (bleno) module loading...');

var bleno = require('bleno');

console.log('BLE (bleno) module loaded.');

var BlenoPrimaryService = bleno.PrimaryService;
var BlenoCharacteristic = bleno.Characteristic;
var BlenoDescriptor = bleno.Descriptor;

var fork = require('child_process').fork;

//De la plataforma Arduino va a llegar lo siguiente:
//  TSWN:50:IndoorTemperature - TemperatureValue
//  TSWN:50:IndoorTemperature - TemperatureThreshold
//que se habra generado a partir de TSWN(pin11, 30, 5, 0)


// Define the configuration and properties of the gateway. This definition will be properly used by the modules
// to create the interfaces and services.
/*var hub = {
    ble: { 
        name: 'DephisitGateway',
	uuid: '0000'
    },
    nodes: {
        arduino1: {
            sensors: {
		PSWN: {
                       PositionValue: {
			    uuid: '0002',
                            descriptorValue: 'TSWN:50:VehicleTemp',

			    readCallFunction: null,
			    readCallbackFunction: null,
			    readCallbackResult: undefined,

			    enableNotificationCallFunction: null,
			    disableNotificationCallFunction: null,

			    onChangeValueCallback: null
                        },
                        PositionThreshold: {
			    uuid: '0003',
                            descriptorValue: 'TSWN:50:VehicleTemp',

			    writeCallFunction: null
                        }
                }
            },
            actuators: {
            }
        }
    }
};*/


console.log('Creating BLE peripheral...');


// ----------------------------- PSWN Thread ----------------------------- //


//var pswnNotifyCallback;

//var pswnCurrentPos = {latitude: 0, longitude: 0};
//var pswnThreshold = 0;

/*var pswnThread = fork('./pswnThread.js');

pswnThread.on('message', function(pswnNewPos) {
  if (pswnNotifyCallback != null && Math.max(Math.abs(pswnNewPos.latitude - pswnCurrentPos.latitude), Math.abs(pswnNewPos.longitude - pswnCurrentPos.longitude)) > pswnThreshold)
  {
	var data = new Buffer(8);
	data.writeFloatLE(pswnCurrentPos.latitude, 0);
	data.writeFloatLE(pswnCurrentPos.longitude, 4);
	pswnNotifyCallback(data);
  }
  pswnCurrentPos = pswnNewPos;
  //console.log('APP << POSITION ' + util.inspect(pswnCurrentPos, false, null));
});*/


// ----------------------------- Position Value Characteristic ----------------------------- //

var TestXXValueCharacteristic = function () {
TestXXValueCharacteristic.super_.call(this, {
    uuid: '0012',
    properties: ['read','notify'],
    descriptors: [
        new BlenoDescriptor({
            uuid: '2901',
            value: 'TSWN:50:VehicleTemp'
        })
    ],
    onReadRequest: function (offset, callback) {
	console.log('TestXXValueCharacteristic onReadRequest - offset=' + offset + ' ,callback=' + callback);

	var data = new Buffer(4);
	data.writeFloatLE(25, 0);
        callback(this.RESULT_SUCCESS, data);
    },
    onSubscribe: function(maxValueSize, updateValueCallback) { 
	console.log('TestXXValueCharacteristic onSubscribe - maxValueSize=' + maxValueSize + ' ,updateValueCallback=' + updateValueCallback);

	//pswnNotifyCallback = updateValueCallback;
    },
    onUnsubscribe: function() {
    	console.log('TestXXValueCharacteristic onUnsubscribe');

	//pswnNotifyCallback = null;
    },
    onNotify: function() {
	console.log('TestXXValueCharacteristic onNotify');
    }
});
};

util.inherits(TestXXValueCharacteristic, BlenoCharacteristic);


// ----------------------------- Position Threshold Characteristic ----------------------------- //

var TestXXThresholdCharacteristic = function () {
TestXXThresholdCharacteristic.super_.call(this, {
    uuid: '0013',
    properties: ['write', 'writeWithoutResponse'],
    descriptors: [
        new BlenoDescriptor({
            uuid: '2901',
            value: 'TSWN:50:VehicleTemp'
        })
    ],
    onWriteRequest: function (data, offset, withoutResponse, callback) {

    	console.log('TestXXThresholdCharacteristic onWriteRequest - data=' + data.toString('hex') + ', offset=' + offset + ', withoutResponse=' + withoutResponse + ', callback=' + callback);
	
	//pswnThreshold = data.readFloatLE(0);
	//console.log('pswnThreshold: ' + pswnThreshold);

	callback(this.RESULT_SUCCESS);
    }
});
};

util.inherits(TestXXThresholdCharacteristic, BlenoCharacteristic);



// ----------------------------- Test Characteristic ----------------------------- //

var TestCharacteristic = function () {
TestCharacteristic.super_.call(this, {
    //uuid: '0012',
    uuid: '0014',
    properties: ['read'],
    descriptors: [
        new BlenoDescriptor({
            uuid: '2901',
            //value: 'TS:23:TestSensor'
	    value: 'LS:23:TestSensor'
        })
    ],
    onReadRequest: function (offset, callback) {
	console.log('TestCharacteristic onReadRequest - offset=' + offset + ' ,callback=' + callback);

	//var data = new Buffer("30");
	var data = new Buffer(4);
	data.writeInt32LE(30, 0);
        callback(this.RESULT_SUCCESS, data);
    }
});
};

util.inherits(TestCharacteristic, BlenoCharacteristic);


// ----------------------------- Create services and advertise them ----------------------------- //

// Creates the service(s) and the characteristics provided by the gateway inside each service
function MyServices() {
        MyServices.super_.call(this, {
            uuid: '0000',
            characteristics: [
		new TestCharacteristic(),
		new TestXXValueCharacteristic(),
		new TestXXThresholdCharacteristic()
            ]
        });
}

util.inherits(MyServices, BlenoPrimaryService);

/*function MyServices2() {
        MyServices2.super_.call(this, {
            uuid: '0000',
            characteristics: [
		new TestCharacteristic()
            ]
        });
}

util.inherits(MyServices2, BlenoPrimaryService);*/

// ----------------------------- BLENO events ----------------------------- //

// Manage the advertising state based on the ble power state.
bleno.on('stateChange', function (state) {
        console.log('on -> stateChange: ' + state);

        if (state === 'poweredOn') {
            bleno.startAdvertising('DephisitGateway', ['0000']);
        } else {
            bleno.stopAdvertising();
        }
});

// A new device gets connected
bleno.on('accept', function () {
        console.log('on -> accept');

        bleno.updateRssi();
});

// Client gets disconnected
bleno.on('disconnect', function () {
        console.log('on -> disconnect');
});

bleno.on('rssiUpdate', function (rssi) {
        console.log('on -> rssiUpdate: ' + rssi);
});

// Inicio del advertising. Inicialización de los servicios
bleno.on('advertisingStart', function (error) {
        console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));

        if (!error) {
            bleno.setServices([
//      new DeviceNameService,
                new MyServices(),
		//new MyServices2()
            ]);
        }
});

// Fin advertising
bleno.on('advertisingStop', function () {
        console.log('on -> advertisingStop');
});

bleno.on('servicesSet', function () {
        console.log('on -> servicesSet');
});


console.log('BLE peripheral created.');



